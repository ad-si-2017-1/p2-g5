Casos de teste
Caso de Teste 1:    
Cenário: Usuário com Nick já definido deseja alterar seu nickname. Verificação de NICK existente;    
Fluxo:     
1 - Usuário digita o comando: nick Gabi;    
2 - O nick informado já existe? - SIM    
3 - O servidor deverá retornar uma mensagem informando que o NICK já existe e não alterará o NICK.    

Validado? -     

Caso de Teste 2:    
Cenário: Usuário tenta expulsar outros usuários do canal. Verificação se o usuário é operador do canal.    
Fluxo:    
1 - Usuário digita o comando: kick canal1 Gabi;    
2 - Usuário solicitante é o operador do canal? - Não    
3 - Servidor deverá retornar uma mensagem informando que o usuário solicitante não é operador do canal e o comando kick não deverá ser realizado.     

Validado? -     

Caso de Teste 3:    
