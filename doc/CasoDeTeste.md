# Casos de teste

## Caso de Teste 1

### Cenário
Usuário com Nick já definido deseja alterar seu nickname. Verificação de NICK existente.

### Fluxo
1. Usuário digita o comando: /nick Gabi;
2. O nick informado já existe? - SIM
3. O servidor deverá retornar uma mensagem informando que o NICK já existe e não alterará o NICK.

<strong>Validado? </strong> -

## Caso de Teste 2

### Cenário
Usuário tenta expulsar outros usuários do canal. Verificação se o usuário é operador do canal.

### Fluxo
1. Usuário digita o comando: /kick canal1 Gabi;
2. Usuário solicitante é o operador do canal? - Não
3. Servidor deverá retornar uma mensagem informando que o usuário solicitante não é operador do canal e o comando kick não deverá ser realizado.

<strong>Validado? </strong> -

## Caso de Teste 3

### Cenário
Usuário tenta convidar outro usuário para o canal. Verificação se o canal é invite-only e se o usuário que convida é operador.

### Fluxo
1. Usuário digita comando: /invite Gabi canal1
2. Canal em questão é invite-only? -Sim
3. Usuário que realizou o convite é operador? -Não
4. Usuário que realizou o convite recebe a mensagem de que é necessária a permissão de administrador para realização de convite neste canal e a solicitação de convite não é realizada.

<strong>Validado? </strong> -

## Caso de Teste 4

### Cenário
Usuároi procura informações de outro usuário, especificando o servidor em que procurar, com o comando WHOIS. É verificada a existência do servidor e se algum usuário é similar à máscara de busca.

### Fluxo
1. Usuário digita o comando: /whois [open.ircnet.net] Gabi
2. Servidor especificado existe? -Sim
3. Usuário especificado existe? -Sim
4. São retornados os dados do usuário especifiado com as mensagens RPL_WHOISUSER RPL_WHOISCHANNELS RPL_WHOISSERVER RPL_ENDOFWHOIS;

<strong>Validado? </strong> -

## Caso de Teste 5

### Cenário
Usuário tenta obter informações sobre o administrador do servidor atual ou especificado

### Fluxo
1. Usuário digita o comando: /admin
2. Servidor retorna com os dados do administrador

<strong>Validado? </strong> -

## Caso de Teste 6

### Cenário
Usuário quer checar se o cliente está inativo.

### Fluxo
1. Usuário digita o comando: /ping gabi
2. Usuário especificado existe? -Sim
3. Mensagem de PONG é retornada pelo usuário pingado.

<strong>Validado? </strong> -


## Caso de Teste 7

### Cenário
Usuário deseja enviar uma mensagem privada para outro usuário.

### Fluxo
1. Usuário digita o comando: /msg gabi oi,tudo bem? podemos conversar em privado?
2. O nick informado existe? -Sim
3. Usuário em questão está away? -Não
4. Mensagem privada é enviada ao usuário.

<strong>Validado? </strong> -

## Caso de Teste 8

### Cenário
Usuário deseja conectar seu servidor a outro servidor remoto.

### Fluxo
1. Usuário digita: /connect open.ircnet.net 5664
2. Usuário que solicitou o comando é operador? -Sim
3. O servidor remoto especificado existe? -Sim
4. Servidor atual se conecta ao servidor remoto.

<strong>Validado? </strong> -

## Caso de Teste 9

### Cenário
Usuário deseja se conectar a outro canal.

### Fluxo
1. Usuário digita: /join canal2
2. Usuário atingiu o limite de canais conectados? -Não
3. Canal especificado existe? -Sim
4. Canal é invite-only? -Não
5. Canal está cheio? -Não
6. Usuário entra no canal.
7. Usuário receve a mensagem RPL_TOPIC que especifica o tópico do canal adentrado.

<strong>Validado? </strong> -

## Caso de Teste 10

### Cenário
Usuário deseja listar todos os usuários do servidor.

### Fluxo
1. Usuário digita: /users open.ircnet.net
2. Servidor especificado existe? -Sim
3. Servidor responde com o seguinte formato: RPL_USERSSTART RPL_USERS RPL_ENDOFUSERS

<strong>Validado? </strong> -

## Caso de Teste 11

### Cenário
Usuário deseja saber quais servidores o servidor especificado ou não conhece.

### Fluxo
1. Usuário digita: /links open.ircnet.net
2. Serviro especificado existe? -Sim
3. Servidor retorna com as mensagems RPL_LINKS RPL_ENDOFLINKS

<strong>Validado? </strong> -
