** FAZER UMA COPIA DESTE DOCUMENTO CASO ESTEJA CONSTRUINDO UM **

## UFG - INSTITUTO DE INFORMATICA - SISTEMAS DE INFORMACAO
## GRUPO 5 / PROJETO 2 - CHAT IRC INTERFACE WEB COM SOCKET.IO

# TITULO DO DOCUMENTO
#### VERSAO XXXX

### PROPOSITO DO DOCUMENTO

- Descricao do objetivo deste documento, definindo como sera feito e qual o impacto na aplicacao.

### CONTEUDO DO DOCUMENTO

- Aqui deverao ser inseridos os topicos, paragrafos e subtopicos referentes ao conteudo do documento.
- Podem ser feitas indicacoes a imagens, sendo [Nome da imagem](link gitlab).

### ANEXOS

- Aqui deverao ser inseridos os nomes dos anexos que este documento faz referencia, contendo sua extensão. Ex.:
    - CasoUso.png - diagrama de caso de uso.

