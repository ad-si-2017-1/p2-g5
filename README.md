# RESUMO DE TRABALHO

# INTEGRANTES

- Gabriela Mansur (DOC)
- **Marcella Toledo** (DOC)
- Julio Cesar (DOC)
- Matheus Assis (DEV)
- Rodrigo Marden (DEV)
- Ygor Alberto (DEV)

# EQUIPES

- Equipe DOC - **desenvolver** a documentacao da aplicacao, seus requisitos, descricoes e todas as diretrizes de auxilio a implemetacao
- Equipe DEV - **implementar** a aplicacao utlizando as ferramentas que melhor convier, sempre atentando ao HTML e o NodeJS.

# OBJETIVOS

- O objetivo do projeto 2 é **desenvolver uma interface WEB utilizando WebSocket (Socket.io) para o protocolo IRC.**
Foi apresentado um protótipo inicial do projeto 3 utilizando REST, deverá ser modificado para suportar WebSocket.

    [Link do Projeto 2 Base](https://gitlab.com/marceloakira/p2/)

# CONSIDERACOES

- Os membros serao avaliados segundo estes criterios: **Relevancia, Conformidade, Criatividade e Erros**.   
- As instrucoes gerais serao as mesmas do projeto 1.
- **Cada integrante trabalhara na sua propria branch**, sendo ela o seu primeiro nome em maiusculo. Ex.:
    - MARCELLA
- Os documentos devem ser feitos na Wiki. Temos o [Manual do Desenvolvedor](https://gitlab.com/ad-si-2017-1/p2-g5/wikis/manual-do-desenvolvedor) e o [Manual do Usuario](https://gitlab.com/ad-si-2017-1/p2-g5/wikis/manual-do-usuario)
- Para cada tarefa será criada uma Inssue e esta deverá ser concluída.
- Qualquer duvida, contatar no Whatsapp e no Telegram.
- Produzam sempre! Nem que seja um esboço de código ou um diagrama básico!

# REFERENCIAS

- [Markdown Sintaxe - .MD](https://sourceforge.net/p/modeloslibreoffice/tickets/markdown_syntax) 
- [Markdown Sintaxe - .MD](https://guides.github.com/features/mastering-markdown/)
- [Npm Javascript Package Manager](https://github.com/npm/npm)
- [Documentação API IRC library](https://node-irc.readthedocs.io/en/latest/API.html#client)
- [Documentação SocketIO](https://socket.io/docs/)