
## UFG - INSTITUTO DE INFORMATICA - SISTEMAS DE INFORMACAO
## GRUPO 5 / PROJETO 2 - CHAT IRC INTERFACE WEB COM SOCKET.IO

## ARQUITETURA DA APLICAÇÃO
#### VERSAO 01

### PROPOSITO DO DOCUMENTO

Este documento visa definir a arquitetura da aplicação do servidor e as tecnologias envolvidas.
O propósito desta aplicação é fornecer uma interface Web utilizando WebSocket (SocketIO) para o protocolo IRC.
A equipe trabalha tendo como base o projeto em REST desenvolvido neste [link](https://ead.inf.ufg.br/mod/resource/view.php?id=58060). 

### CONTEUDO DO DOCUMENTO

#### TECNOLOGIAS UTILIZADAS E DEFINIÇÕES

- Orientação a eventos:
- Sincrono e assincrono:
- HTML:
- WebService:
- Json:
- NodeJS:
- IRC:

### ANEXOS

- Aqui deverao ser inseridos os nomes dos anexos que este documento faz referencia, contendo sua extensão. Ex.:
    - CasoUso.png - diagrama de caso de uso.

