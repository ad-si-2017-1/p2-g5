//var fs = require('fs')
//    , socketio = require('socket.io');
var express = require('express');  // módulo express
//var app = express();		   // objeto express
var bodyParser = require('body-parser');  // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var irc = require('irc');


var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var path = require('path');	// módulo usado para lidar com caminhos de arquivos

var proxies = {}; // mapa de proxys
var proxy_id = 0;
var canal = '#ad-si-2017-1';
var servidor = 'localhost';
var nick = 'ygorats';


app.get('/', function (req, res) {
	if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
		proxy_id++;
		var p =	proxy(	proxy_id,
				req.cookies.servidor,
				req.cookies.nick, 
				req.cookies.canal);
		res.cookie('id', proxy_id);
	  	res.sendFile(path.join(__dirname, '/index.html'));
	}
	else {
		res.sendFile(path.join(__dirname, '/login.html'));
	}
});

app.get('/obter_mensagem/:timestamp', function (req, res) {
  var id = req.cookies.id;
  res.append('Content-type', 'application/json');
  res.send(proxies[id].cache);
});

app.post('/gravar_mensagem', function (req, res) {
  proxies[req.cookies.id].cache.push(req.body);
  var irc_client = proxies[req.cookies.id].irc_client;
  irc_client.say(irc_client.opt.channels[0], req.body.msg );
  res.end();
});

app.get('/mode/:usuario/:args', function (req, res){
  var usuario = req.params.usuario;
  var args = req.params.args;
  //var retorno = '{"usuario":'+usuario+','+
  //		  '"args":"'+args+'}';
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", usuario, args);
  res.send(retorno);
});

app.get('/mode/', function (req, res){
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", req.cookies.nick);
  res.send(retorno);
});


app.post('/login', function (req, res) { 
   res.cookie('nick', req.body.nome);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});

io.on('connection', function (socket) {
	/*if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
		proxy_id++;
		var p =	proxy(	proxy_id,
				req.cookies.servidor,
				req.cookies.nick, 
				req.cookies.canal);
		res.cookie('id', proxy_id);
	  	res.sendFile(path.join(__dirname, '/index.html'));
	}
	else {
		res.sendFile(path.join(__dirname, '/login.html'));
	}*/
	socket.emit('news', { hello: 'world' });
	socket.on('my other event', function (data) {
		console.log(data);
	});
	// Função de processamento de mensagens enviadas pelo usuário
	socket.on('message', function (msg) {
		console.log('Message Received: ', msg);
		irc_client.say(canal, msg );
	});

});

function proxy(id, servidor, nick, canal){
	var cache = []; // cache de mensagens

	// Criação do cliente IRC
	var irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal],});

	// Adiciona no cliente IRC do proxy uma escuta para o envio de mensagens
	irc_client.addListener('message'+canal, function (from, message) {
	    console.log(from + ' => '+ canal +': ' + message);
	    cache.push(	{"timestamp":Date.now(), 
			"nick":from,
			"msg":message} );
	});
	
	// disparado quando o cliente estiver conectado
	irc_client.addListener('registered', function(message) {
	  console.log('conectado:'+JSON.stringify(message));
	  socket.emit('message', 'conectado em: irc://'+nick+'+@'+servidor+'/'+canal);
	});
	
	// Disparado quando ocorre um erro
	irc_client.addListener('error', function(message) {
	    console.log('error: ', message);
	});
	
	// Disparado na mudança de modo
	irc_client.addListener('mode', function(message) {
	    console.log('mode: ', message);
	});
	
	// Adiciona este proxy na lista de proxies.
	proxies[id] = { "cache":cache, "irc_client":irc_client  };
}

function conexao(){

}

app.listen(3000, function () {				
  console.log('Example app listening on port 3000!');	
});
