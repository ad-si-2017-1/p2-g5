

## UFG - INSTITUTO DE INFORMATICA - SISTEMAS DE INFORMACAO
## GRUPO 5 / PROJETO 2 - CHAT IRC INTERFACE WEB COM SOCKET.IO

## TITULO DO DOCUMENTO
#### VERSAO XXXX

### PROPOSITO DO DOCUMENTO

Este documento visa definir os requisitos que nortearão a construção do Chat IRC Web.
Serão representados neste documento os eventos tratados, os comandos implementados
e a definição da interface.

### CONTEUDO DO DOCUMENTO

Requisitos da Aplicação
- [RFXX] O servidor será capaz de suportar N canais. Cada canal comportará N conexões cliente.
- [RFXX] O servidor, quando possivel, será capaz de listar os canais criados nele.
- [RFXX] O servidor terá um administrador que é um cliente. Este terá privilégios.
- [RFXX] O servidor será capaz de manter uma lista atualizada dentro de canal de quais clientes estão conectados.
- [RFXX] O servidor enviará uma mensagem do dia a todos do canal. Essa mensagem é definida pelo cliente administrador.
- [RFXX] O cliente se conectará ao servidor por um nick e pelo nome do canal. Este cliente terá também um nome real.
- [RFXX] O cliente poderá sair do canal, mas não necessariamente do servidor.
- [RFXX] O cliente poderá enviar mensagem no canal a todos.
- [RFXX] O cliente poderá enviar mensagem privada a um outro cliente utilizando seu nick.
- [RFXX] O cliente poderá requisitar o nome real de um outro cliente utlizando seu nick.
- [RFXX] O cliente poderá ser banido do servidor ou do canal por outro cliente com privilégios de administrador.

Tratamento de Eventos (segundo API node-irc - ver [documentação original](https://node-irc.readthedocs.io/en/latest/API.html#events))
- 'registered': function (message) { }
Emitida pelo servidor quando você conecta ao canal. A mensagem é "Conectado".

- 'motd': function (motd) { }
Emitida quando o servidor manda a mensagem do dia aos clientes.

'names'
function (channel, nicks) { }

CONTINUAR A TRATAR ESTAS INFORMAÇÕES.

Emitted when the server sends a list of nicks for a channel (which happens immediately after joining and on request. The nicks object passed to the callback is keyed by nick names, and has values ‘’, ‘+’, or ‘@’ depending on the level of that nick in the channel.

'names#channel'
function (nicks) { }

As per ‘names’ event but only emits for the subscribed channel.

'topic'
function (channel, topic, nick, message) { }

Emitted when the server sends the channel topic on joining a channel, or when a user changes the topic on a channel. See the raw event for details on the message object.

'join'
function (channel, nick, message) { }

Emitted when a user joins a channel (including when the client itself joins a channel). See the raw event for details on the message object.

'join#channel'
function (nick, message) { }

As per ‘join’ event but only emits for the subscribed channel. See the raw event for details on the message object.

'part'
function (channel, nick, reason, message) { }

Emitted when a user parts a channel (including when the client itself parts a channel). See the raw event for details on the message object.

'part#channel'
function (nick, reason, message) { }

As per ‘part’ event but only emits for the subscribed channel. See the raw event for details on the message object.

'quit'
function (nick, reason, channels, message) { }

Emitted when a user disconnects from the IRC, leaving the specified array of channels. See the raw event for details on the message object.

'kick'
function (channel, nick, by, reason, message) { }

Emitted when a user is kicked from a channel. See the raw event for details on the message object.

'kick#channel'
function (nick, by, reason, message) { }

As per ‘kick’ event but only emits for the subscribed channel. See the raw event for details on the message object.

'kill'
function (nick, reason, channels, message) { }

Emitted when a user is killed from the IRC server. channels is an array of channels the killed user was in which are known to the client. See the raw event for details on the message object.

'message'
function (nick, to, text, message) { }

Emitted when a message is sent. to can be either a nick (which is most likely this clients nick and means a private message), or a channel (which means a message to that channel). See the raw event for details on the message object.

'message#'
function (nick, to, text, message) { }

Emitted when a message is sent to any channel (i.e. exactly the same as the message event but excluding private messages. See the raw event for details on the message object.

'message#channel'
function (nick, text, message) { }

As per ‘message’ event but only emits for the subscribed channel. See the raw event for details on the message object.

'selfMessage'
function (to, text) { }

Emitted when a message is sent from the client. to is who the message was sent to. It can be either a nick (which most likely means a private message), or a channel (which means a message to that channel).

'notice'
function (nick, to, text, message) { }

Emitted when a notice is sent. to can be either a nick (which is most likely this clients nick and means a private message), or a channel (which means a message to that channel). nick is either the senders nick or null which means that the notice comes from the server. See the raw event for details on the message object.

'ping'
function (server) { }

Emitted when a server PINGs the client. The client will automatically send a PONG request just before this is emitted.

'pm'
function (nick, text, message) { }

As per ‘message’ event but only emits when the message is direct to the client. See the raw event for details on the message object.

'ctcp'
function (from, to, text, type, message) { }

Emitted when a CTCP notice or privmsg was received (type is either ‘notice’ or ‘privmsg’). See the raw event for details on the message object.

'ctcp-notice'
function (from, to, text, message) { }

Emitted when a CTCP notice was received. See the raw event for details on the message object.

'ctcp-privmsg'
function (from, to, text, message) { }

Emitted when a CTCP privmsg was received. See the raw event for details on the message object.

'ctcp-version'
function (from, to, message) { }

Emitted when a CTCP VERSION request was received. See the raw event for details on the message object.

'nick'
function (oldnick, newnick, channels, message) { }

Emitted when a user changes nick along with the channels the user is in. See the raw event for details on the message object.

'invite'
function (channel, from, message) { }

Emitted when the client receives an /invite. See the raw event for details on the message object.

'+mode'
function (channel, by, mode, argument, message) { }

Emitted when a mode is added to a user or channel. channel is the channel which the mode is being set on/in. by is the user setting the mode. mode is the single character mode identifier. If the mode is being set on a user, argument is the nick of the user. If the mode is being set on a channel, argument is the argument to the mode. If a channel mode doesn’t have any arguments, argument will be ‘undefined’. See the raw event for details on the message object.
'-mode'
function (channel, by, mode, argument, message) { }

Emitted when a mode is removed from a user or channel. channel is the channel which the mode is being set on/in. by is the user setting the mode. mode is the single character mode identifier. If the mode is being set on a user, argument is the nick of the user. If the mode is being set on a channel, argument is the argument to the mode. If a channel mode doesn’t have any arguments, argument will be ‘undefined’. See the raw event for details on the message object.
'whois'
function (info) { }

Emitted whenever the server finishes outputting a WHOIS response. The information should look something like:

{
    nick: "Ned",
    user: "martyn",
    host: "10.0.0.18",
    realname: "Unknown",
    channels: ["@#purpledishwashers", "#blah", "#mmmmbacon"],
    server: "*.dollyfish.net.nz",
    serverinfo: "The Dollyfish Underworld",
    operator: "is an IRC Operator"
}
'channellist_start'
function () {}

Emitted whenever the server starts a new channel listing

'channellist_item'
function (channel_info) {}

Emitted for each channel the server returns. The channel_info object contains keys ‘name’, ‘users’ (number of users on the channel), and ‘topic’.

'channellist'
function (channel_list) {}

Emitted when the server has finished returning a channel list. The channel_list array is simply a list of the objects that were returned in the intervening channellist_item events.

This data is also available via the Client.channellist property after this event has fired.

'raw'
function (message) { }

Emitted when ever the client receives a “message” from the server. A message is basically a single line of data from the server, but the parameter to the callback has already been parsed and contains:

message = {
    prefix: "The prefix for the message (optional)",
    nick: "The nickname portion of the prefix (optional)",
    user: "The username portion of the prefix (optional)",
    host: "The hostname portion of the prefix (optional)",
    server: "The servername (if the prefix was a servername)",
    rawCommand: "The command exactly as sent from the server",
    command: "Human readable version of the command",
    commandType: "normal, error, or reply",
    args: ['arguments', 'to', 'the', 'command'],
}
You can read more about the IRC protocol by reading RFC 1459

'error'
function (message) { }

Emitted when ever the server responds with an error-type message. The message parameter is exactly as in the ‘raw’ event.

'action'
function (from, to, text, message) { }

Emitted whenever a user performs an action (e.g. /me waves). The message parameter is exactly as in the ‘raw’ event.




### ANEXOS

- Aqui deverao ser inseridos os nomes dos anexos que este documento faz referencia, contendo sua extensão. Ex.:
    - CasoUso.png - diagrama de caso de uso.
